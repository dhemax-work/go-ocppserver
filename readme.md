# Configuración OCPP Server

Para poder iniciar el servidor de manera correcta, hace falta seguir estas instrucciones a la hora de levantar un entorno de producción, como desarrollo.

El **Open Charge Point Protocol** (**OCPP**), que se podría traducir como **protocolo abierto de punto de carga**, es un [protocolo de aplicación](https://es.wikipedia.org/wiki/Protocolo_de_comunicaciones "Protocolo de comunicaciones") para la comunicación entre las [estaciones de carga](https://es.wikipedia.org/wiki/Estaci%C3%B3n_de_carga "Estación de carga") de [vehículos eléctricos](https://es.wikipedia.org/wiki/Veh%C3%ADculo_el%C3%A9ctrico_de_bater%C3%ADa "Vehículo eléctrico de batería") y un sistema central de gestión, también conocido como red de estaciones de carga, similar al sistema que conforman los [teléfonos móviles](https://es.wikipedia.org/wiki/Tel%C3%A9fonos_m%C3%B3viles "Teléfonos móviles") y las redes de teléfonos móviles.

Para nuestros propósitos estamos configurando un servidor `'central system'` que conectara a cada máquina o `'charge point'`con este servidor.

De esta manera podemos obtener y manipular la información de las maquinas conectadas, desconectarlas y ejecutar comandos de manera remota (por API) o desde consola, previamente configurándolos.

# Dependencias
Para iniciar el servidor de ocpp necesitamos unas dependencias de GO, estas se instalan con los siguientes comandos.

    go get "github.com/briandowns/spinner"  
    go get "github.com/eduhenke/go-ocpp"
    go get "github.com/rs/cors"
    go get "github.com/marcusolsson/tui-go"
    go get "github.com/googollee/go-socket.io"

## Reemplazar librería

Al descargar el paquete de `eduhenke`, necesitamos 'reemplazar' su librería con la creada por nosotros.
En nuestro árbol de archivos encontramos `'replace_cs.example'`copias su contenido, buscar en la librería `'GOPATH <'ocpp-server'> / src / github.com / eduhenke / go-ocpp / cs / cs.go` y pegamos el contenido en el archivo cs.go.

En caso de windows, GOPATH es  `users/usuario/go/src`para saber la ruta de su GOPATH ejecutar en consola el comando `go env`.


## Compilar e iniciar

Vamos a la carpeta `src` y ejecutamos en consola `go build ./` posteriormente ejecutamos con `src.exe` o `./src` según el sistema operativo.


## Funciones

    ExecuteNoUICommand(command string);
   Para crear comandos personalizados debemos ir a la función mencionada  y agregar: 

    else if strings.Compare("MI_COMANDO", command) == 0 {  
       fmt.Println("¡Hola Mundo!")
       executarAlgunaFuncion();  
    }

## Estructuras JSON

 Para ejecutar alguna función JSON debemos crear una estructura normal de GO, agregando su parámetro de como deberías llamado en raw.  
  
Ejemplo:  

    type Test struct{  
	    TestID int `json:"testID"`
    }  

  
Para esta función, al usar raw (ej: en Postman) el nombre del campo será `'testID'` y el valor cual se desea asignarle.

## Helpers

     CallClear()
     *Borrar el log de consola.*
    
     ToTimestamp(date string);
     *De string a timestamp*
