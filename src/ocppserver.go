package main

import (
	"encoding/xml"
	"errors"
	"fmt"
	"github.com/eduhenke/go-ocpp"
	"github.com/eduhenke/go-ocpp/cs"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpreq"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpresp"
	"github.com/eduhenke/go-ocpp/messages/v1x/csreq"
	"github.com/eduhenke/go-ocpp/messages/v1x/csresp"
	"log"
	"math/rand"
	"os"
	"time"
)

/* VARIABLES */
//centralSystem ... main connection to the lib
var centralSystem cs.CentralSystem

//ChargepointVersion ... set the charge point version
var ChargepointVersion = ocpp.V16

var MachineStatusMessage = "Offline"

//StartServer ...
func StartOCPPServer(port string) {
	fmt.Printf("\n== [Starting OCPP Server] ==\n")
	time.Sleep(time.Second * 2)

	AddOCPPLog("OCPP", "== [Starting OCPP Server] ==")
	time.Sleep(time.Second * 2)

	ocpp.SetDebugLogger(log.New(os.Stdout, "[DEBUG]: ", log.Ltime))
	ocpp.SetErrorLogger(log.New(os.Stderr, "[ERROR]: ", log.Ltime))

	centralSystem = cs.New()

	var serverPort = fmt.Sprintf(":%v", port)

	fmt.Printf("\n== [Preparing Central System] ==\n")
	time.Sleep(time.Second * 2)

	go centralSystem.Run(serverPort, func(req cpreq.ChargePointRequest, cpID string) (cpresp.ChargePointResponse, error) {

		//THE SERVER IS STARTED
		AddOCPPLog("OCPP", fmt.Sprintf("START: [Connection request of %s]", cpID))
		fmt.Printf("START: [Connection request of %s]", cpID)

		switch req := req.(type) {

		/*
			Status:
			- Accepted 	= Charge point is accepted by Central System.
			- Pending 	= Central System is not yet ready to accept the Charge Point. Central System may send messages to retrieve information or prepare the Charge Point
			- Rejected 	= Charge point is not accepted by Central System.
		*/
		case *cpreq.BootNotification:

			return &cpresp.BootNotification{
				XMLName:     xml.Name{},
				Status:      "Accepted",
				CurrentTime: time.Now(),
				Interval:    25,
			}, nil

		case *cpreq.MeterValues:
			connectorID := req.ConnectorId
			transactionID := req.TransactionId
			text := fmt.Sprintf("Connector: %v TranID: %v \n", connectorID, transactionID)
			//AddOCPPLog("OCPP", text)
			cs.AddLog(cpID, text)
			return &cpresp.MeterValues{}, nil

		case *cpreq.Heartbeat:
			fmt.Printf("\n\n == [LATIDO: %s]==\n\n", cpID)

			if !cs.IsMachineConnect(cpID) {
				cs.ConnectedMachines = append(cs.ConnectedMachines, cpID)
			}
			cs.AddLog(cpID, "HeartBeat <3")
			return &cpresp.Heartbeat{CurrentTime: time.Now()}, nil

		case *cpreq.StatusNotification:
			if req.Status == "Available" {
				fmt.Printf("[INFO] El punto de carga esta disponible \n")
			} else if req.Status == "Preparing" {
				fmt.Printf("[INFO] El punto de carga esta preparado \n")
			} else if req.Status == "Charging" {
				fmt.Printf("[INFO] El punto de carga esta cargando \n")
			} else {
				fmt.Printf("[ERROR] El punto de carga no esta disponible \n")
			}
			//MachineStatusMessage = req.Status
			cs.AddLog(cpID, req.Status)
			return &cpresp.StatusNotification{}, nil

		case *cpreq.FirmwareStatusNotification:
			//message := req.FirmwareStatusNotification
			//fmt.Printf("Message: %v", message)
			return nil, nil
		case *cpreq.Authorize:

			t := time.Now().Local().Add(time.Minute * 2)
			fmt.Printf("[INFO] Tiempo %s\n", t)

			if req.IdTag != "" {
				fmt.Printf("[INFO] Tarjeta aceptada...\n")
				status := &cpresp.Authorize{IdTagInfo: &cpresp.IdTagInfo{ExpiryDate: &t, ParentIdTag: req.IdTag, Status: "Accepted"}}
				fmt.Printf("[INFO STATUS] %v\n", status)
				cs.AddLog(cpID, fmt.Sprintf("%v", req))
				AsyncTask()
				return status, nil
			}

			fmt.Printf("[INFO] Tarjeta NO aceptada...\n")

			return &cpresp.Authorize{IdTagInfo: &cpresp.IdTagInfo{ExpiryDate: &t, ParentIdTag: req.IdTag, Status: "Blocked"}}, nil
		default:
			fmt.Printf("[ERROR]: Accion no soportada: %s\n", req.Action())

			return nil, errors.New("la respuesta no es soportada")
		}
	})

	fmt.Printf("\n== [Central System Started] ==\n")
	time.Sleep(time.Second * 2)
}

// AsyncTask ... Async Task for start transaction after x seconds
func AsyncTask() <-chan int32 {
	fmt.Print("[INFO] Async Task Started \n")
	r := make(chan int32)

	go func() {
		defer close(r)

		fmt.Print("[INFO] Async Task 5s \n")
		time.Sleep(time.Second * 5)
		//RemoteStart(cpIDvar, CardID)
		r <- rand.Int31n(100)

	}()
	return r
}

/* MACHINE FUNCTIONS */

// UnlockConnector ... [READY] [1]
func UnlockConnector(cpID string, connectorID int) Response {
	fmt.Printf("\n===[UNLOCK CONNECTOR #%v FOR %v]===\n", connectorID, cpID)
	var respo Response

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.UnlockConnector{
			ConnectorId: connectorID,
		})

		if err == nil {
			resp := cpResp.(*csresp.UnlockConnector)
			fmt.Printf("== UC RESPONSE: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("== [ERROR] Not Accepted \n")
				respo = Response{
					Code:    201,
					Message: fmt.Sprintf("Unlocking connector [%v] rejected", connectorID),
				}
			} else {
				fmt.Printf("== [READY] Accepted \n")
				respo = Response{
					Code:    201,
					Message: fmt.Sprintf("Unlocking connector [%v] accepted", connectorID),
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    300,
			Message: err.Error(),
		}
	}
	return respo
}

// ClearCache ... [READY] [2]
func ClearCache(cpID string) Response {
	fmt.Printf("\n===[CLEARING CACHE FOR %v]===\n", cpID)
	var response Response

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.ClearCache{})

		if err == nil {
			resp := cpResp.(*csresp.ClearCache)
			fmt.Printf("RESPUESTA CACHE: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("[ERROR] Not Accepted \n")
				response = Response{
					Code:    201,
					Message: "Clear Cache Reject",
				}
			} else {
				response = Response{
					Code:    200,
					Message: "Clear Cache Accepted",
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		response = Response{
			Code:    300,
			Message: err.Error(),
		}
	}
	return response
}

//ResetMachine ... [READY] [3]
func ResetMachine(cpID string, ResetType string) Response {
	fmt.Printf("\n===[RESETING MACHINE (%v)]===\n", cpID)
	var respo = Response{
		Code:    500,
		Message: "Method Crash",
	}

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		if ResetType == "Hard" || ResetType == "Soft" {
			cpResp, err := cpService.Send(&csreq.Reset{
				/*
					Hard = Full Reset
					Soft = Return to initial status (stop any transaction in progress)
				*/
				Type: ResetType,
			})
			if err == nil {
				resp := cpResp.(*csresp.Reset)
				fmt.Printf("== RESET TYPE: %v\n", ResetType)
				fmt.Printf("== RESET RESPONSE: %v\n", resp)
				if resp.Status != "Accepted" {
					fmt.Printf("== RESET [ERROR] reject. \n")
					respo = Response{
						Code:    201,
						Message: "Rejected Reset",
					}
				} else {
					fmt.Printf("== RESET succcessful. \n")

					respo = Response{
						Code:    200,
						Message: fmt.Sprintf("Reset Complete, Status: %s", resp.Status),
					}
				}
				cs.AddLog(cpID, fmt.Sprintf("%v", resp))
			} else {
				fmt.Printf("[ERROR] %s \n", err)
				respo = Response{
					Code:    301,
					Message: err.Error(),
				}
			}
		} else {
			respo = Response{
				Code:    300,
				Message: fmt.Sprintf("El tipo de reinicio [%s] no es valido", ResetType),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    0,
			Message: "Charge Point is not available",
		}
	}
	return respo
}

// ChangeConfiguration ... [READY] [4]
func ChangeConfiguration(cpID string, key string, value string) Response {
	fmt.Printf("\n===[MACHINE (%v) GET CONFIG]===\n", cpID)
	var respo Response

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.ChangeConfiguration{
			Key:   key,
			Value: value,
		})

		if err == nil {
			resp := cpResp.(*csresp.ChangeConfiguration)
			fmt.Printf("== ChangeConfiguration Response: %v\n", resp)

			if resp.Status != "Accepted" {
				fmt.Printf("== ChangeConfiguration Response: REJECT REQUEST\n")
				respo = Response{
					Code:    201,
					Message: "Change Reject",
				}
			} else {
				fmt.Printf("== ChangeConfiguration Response: ACCEPTED REQUEST\n")
				respo = Response{
					Code:         200,
					Message:      "Change Accepted",
					JSONResponse: resp,
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("== ChangeConfiguration Response: [ERROR] %s \n", err)
			respo = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("== ChangeConfiguration Response: [ERROR] %s \n", err)
		respo = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return respo
}

//GetConfiguration ... [READY] [5]
func GetConfiguration(cpID string, key []string) Response {
	fmt.Printf("\n===[GETTING CONFIG (%v)]===\n", cpID)
	var respo Response
	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")
	if err == nil {
		cpResp, err := cpService.Send(&csreq.GetConfiguration{
			Key: key,
		})
		resp := cpResp.(*csresp.GetConfiguration)
		if err == nil {
			fmt.Printf("== GET CONFIG: %v\n", resp)
			if len(resp.UnknownKey) > 0 {
				fmt.Printf("== GET CONFIG UNKNOWN KEY \n")
				respo = Response{
					Code:         201,
					Message:      "Unknown Key",
					JSONResponse: resp.UnknownKey,
				}
			} else {
				fmt.Printf("== GET CONFIG SUCCESS: %v", resp.ConfigurationKey)
				respo = Response{
					Code:         200,
					Message:      "Getting the Key-Value",
					JSONResponse: resp.ConfigurationKey,
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			respo = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    0,
			Message: "Error punto de carga no operativo",
		}
	}
	return respo
}

//GetMachineStatus ... [READY] [6]
func GetMachineStatus(cpID string) Response {
	var respo Response

	if cs.IsMachineConnect(cpID) {
		fmt.Printf("\n===[MACHINE (%v) STATUS ONLINE]===", cpID)
		respo = Response{
			Code:    200,
			Message: MachineStatusMessage,
		}
	} else {
		fmt.Printf("\n===[MACHINE (%v) REQUEST OFFLINE]===", cpID)
	}
	return respo
}

// GetDiagnostic ... [READY] [7] Central System can request a Charge Point for diagnostic information
func GetDiagnostic(cpID string, location string, retries int, retryinterval int, startt time.Time, endt time.Time) Response {
	fmt.Printf("\n===[DIAGNOSTIC FOR %v]===\n", cpID)
	var response Response

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		// ? URL Required. This contains the location (directory) where the diagnostics file shall be uploaded to
		cpResp, err := cpService.Send(&csreq.GetDiagnostics{
			Location:      location,
			Retries:       retries,
			RetryInterval: retryinterval,
			StartTime:     &startt,
			StopTime:      &endt,
		})
		if err == nil {
			resp := cpResp.(*csresp.GetDiagnostics)
			fmt.Printf("RESPUESTA DIAGNOSTIC: %v\n", resp)
			if resp.FileName != "" {
				fmt.Printf("[ERROR] Error de obtencion de datos \n")
				response = Response{
					Code:    201,
					Message: "No se pueden obtener los datos",
				}
			} else {
				response = Response{
					Code:         200,
					Message:      "Datos obtenidos: Archivo[" + resp.FileName + "]",
					JSONResponse: resp.XMLName.Space,
				}
				cs.AddLog(cpID, fmt.Sprintf("%v", resp))
			}
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			response = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return response
}

// RemoteStop ... [READY] [8] RemoteTransaction stop
func RemoteStop(cpID string, transaction int32) Response {
	var respo Response

	fmt.Printf("\n===[REMOTE STOP FOR #%v]===\n", cpID)

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")
	if err == nil {
		cpResp, err := cpService.Send(&csreq.RemoteStopTransaction{
			TransactionId: transaction,
		})

		if err == nil {
			resp := cpResp.(*csresp.RemoteStopTransaction)
			fmt.Printf("RESPUESTA STOP: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("[ERROR] La transaccion fue rechazada. \n")
				respo = Response{
					Code:    201,
					Message: fmt.Sprintf("Detencion remota rechazada, Status: %s", resp.Status),
				}
			} else {
				respo = Response{
					Code:    200,
					Message: fmt.Sprintf("Detencion remota aceptada, Status: %s", resp.Status),
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    300,
				Message: "Hubo un error inesperado",
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    0,
			Message: "Error punto de carga no operativo",
		}
	}
	return respo
}

// GetLocalList ... [READY] [9]
func GetLocalList(cpID string) Response {
	fmt.Printf("\n===[LOCAL LIST FOR #%v]===\n", cpID)
	var response Response

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.GetLocalListVersion{})
		if err == nil {
			resp := cpResp.(*csresp.GetLocalListVersion)
			fmt.Printf("RESPUESTA GET LIST: %v\n", resp)
			fmt.Printf("LISTA VERSIONES: %v\n", resp.ListVersion)
			response = Response{
				Code:         200,
				Message:      "List of version",
				JSONResponse: resp.ListVersion,
			}
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		response = Response{
			Code:    300,
			Message: err.Error(),
		}
	}

	return response
}

// TriggerMessage ... [READY] [10]
func TriggerMessage(cpID string, connector int, requestMessage string) Response {
	fmt.Printf("\n===[TRG MESSAGE FOR #%v]===\n", cpID)
	var response Response

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")
	if err == nil {
		cpResp, err := cpService.Send(&csreq.TriggerMessage{
			ConnectorId:      connector,
			RequestedMessage: requestMessage,
		})
		if err == nil {
			resp := cpResp.(*csresp.TriggerMessage)
			fmt.Printf("== TRG MSG: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("[ERROR] Not Accepted \n")
				response = Response{
					Code:    201,
					Message: "Not accepted",
				}
			} else {
				response = Response{
					Code:    200,
					Message: resp.Status,
				}
			}
		} else {
			response = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		response = Response{
			Code:    300,
			Message: err.Error(),
		}
	}
	return response
}

// RemoteStart ... [READY] [11]
func RemoteStart(data RemoteStartStructure) Response {
	fmt.Printf("\n===[REMOTE START REQUEST #%v]===\n", data.MachineId)
	var response Response

	cpService, err := centralSystem.GetServiceOf(data.MachineId, ChargepointVersion, "")

	if err == nil {
		tag := data.TagID

		/*
			Absolute: Schedule periods are relative to a fixed point in time defined in the schedule.
			Recurring: The schedule restarts periodically at the first schedule period.
			Relative: Schedule periods are relative to a situation specific start point (such as the start of a session) that is determined by the charge point.
		*/

		cp := csreq.ChargingProfile{
			TransactionId:       data.TransactionId,     //int (optional)
			ChargingProfileId:   data.ChargingProfileId, //integer (unique)
			ChargingProfileKind: data.ChargingProfileKind,
			//ChargingProfilePurpose: "TxDefaultProfile",
			ChargingSchedule: &csreq.ChargingSchedule{
				Duration:         data.ChargingscheduleDuration,         //int
				ChargingRateUnit: data.ChargingscheduleChargingrateunit, //W, A
				ChargingSchedulePeriod: []*csreq.ChargingSchedulePeriodItems{
					{
						StartPeriod:  data.ChargingscheduleperiodStartperiod,
						Limit:        data.ChargingscheduleperiodLimit,
						NumberPhases: data.ChargingscheduleperiodNumberphases,
					},
				},
				//MinChargingRate: 300,
				//StartSchedule:   &time.Time{},
			},
			RecurrencyKind: data.ChargingRecurrencyKind, //Daily, Weekly
			StackLevel:     data.ChargingStackLevel,     //int > 0 :: 100
			//ValidFrom:      &time.Time{},
			//ValidTo:        &time.Time{},
		}

		cpResp, err := cpService.Send(&csreq.RemoteStartTransaction{
			IdTag:           tag,
			ConnectorId:     1,
			ChargingProfile: &cp,
		})

		if err == nil {
			resp := cpResp.(*csresp.RemoteStartTransaction)
			fmt.Printf("[REMOTE START] Says: %v\n", resp)
			response = Response{
				Code:         200,
				Message:      resp.Status,
				JSONResponse: resp,
			}
		} else {
			fmt.Printf("[ERROR] %s", err)
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}

	return response
}

// RemoteStart ... [READY] [12]
func UpdateFirmware(data UpdateFirmwareStructure) Response {
	fmt.Printf("\n===[UPDATE REQUEST FOR #%v]===\n", data.MachineId)
	var response Response

	cpService, err := centralSystem.GetServiceOf(data.MachineId, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.UpdateFirmware{
			Location:      data.Location, //URL
			Retries:       data.Retries,
			RetrieveDate:  data.RetrieveDate,
			RetryInterval: data.RetryInterval,
		})

		if err == nil {
			resp := cpResp.(*csresp.UpdateFirmware)
			fmt.Printf("== UPDATE FIRMWARE: %v\n", resp)
			response = Response{
				Code:         200,
				Message:      "Started?",
				JSONResponse: resp,
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}

	return response
}

// RemoteStart ... [READY] [13]
func SetChargingProfile(data RemoteStartStructure) Response {
	fmt.Printf("\n===[UPDATE REQUEST FOR #%v]===\n", data.MachineId)
	var response Response

	cpService, err := centralSystem.GetServiceOf(data.MachineId, ChargepointVersion, "")

	cp := csreq.CsChargingProfiles{
		TransactionId:       data.TransactionId,     //int (optional)
		ChargingProfileId:   data.ChargingProfileId, //integer (unique)
		ChargingProfileKind: data.ChargingProfileKind,
		//ChargingProfilePurpose: "TxDefaultProfile",
		ChargingSchedule: &csreq.ChargingSchedule{
			Duration:         data.ChargingscheduleDuration,         //int
			ChargingRateUnit: data.ChargingscheduleChargingrateunit, //W, A
			ChargingSchedulePeriod: []*csreq.ChargingSchedulePeriodItems{
				{
					StartPeriod:  data.ChargingscheduleperiodStartperiod,
					Limit:        data.ChargingscheduleperiodLimit,
					NumberPhases: data.ChargingscheduleperiodNumberphases,
				},
			},
			//MinChargingRate: 300,
			//StartSchedule:   &time.Time{},
		},
		RecurrencyKind: data.ChargingRecurrencyKind, //Daily, Weekly
		StackLevel:     data.ChargingStackLevel,     //int > 0 :: 100
		//ValidFrom:      &time.Time{},
		//ValidTo:        &time.Time{},
	}

	if err == nil {
		cpResp, err := cpService.Send(&csreq.SetChargingProfile{
			ConnectorId:        2,
			CsChargingProfiles: &cp,
		})

		if err == nil {
			resp := cpResp.(*csresp.UpdateFirmware)
			fmt.Printf("== SETTING PROFILE: %v\n", resp)
			response = Response{
				Code:         200,
				Message:      "Profile Set",
				JSONResponse: resp,
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return response
}

//ReserveNow ... [14] [READY]
func ReserveNow(data ReserveNowStructure) Response {
	var response Response
	fmt.Printf("\n===[RESERVING FOR #%v]===\n", data.MachineID)

	cpService, err := centralSystem.GetServiceOf(data.MachineID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.ReserveNow{
			ConnectorId:   data.ConnectorID,
			ExpiryDate:    data.ExpiryDate,
			IdTag:         data.IdTag,
			ParentIdTag:   data.ParentIdTag,
			ReservationId: data.ReservationID,
		})

		if err == nil {
			resp := cpResp.(*csresp.ReserveNow)
			cs.AddLog(data.MachineID, fmt.Sprintf("%v", resp))
			fmt.Printf("== RESPUESTA MENSAJE: %v\n", resp)

			if resp.Status == "Accepted" {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			} else {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return response
}

//CancelReservation ... [14]
func CancelReservation(data ReserveNowStructure) Response {
	var response Response
	fmt.Printf("\n===[RESERVING FOR #%v]===\n", data.MachineID)

	cpService, err := centralSystem.GetServiceOf(data.MachineID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.CancelReservation{
			ReservationId: data.ReservationID,
		})

		if err == nil {
			resp := cpResp.(*csresp.ReserveNow)
			cs.AddLog(data.MachineID, fmt.Sprintf("%v", resp))
			fmt.Printf("== RESPUESTA MENSAJE: %v\n", resp)

			if resp.Status == "Accepted" {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			} else {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return response
}

func GetCompositeSchedule(cpID string, rateUnit string, connectorID int, duration int) Response {
	var response Response
	fmt.Printf("\n===[GET SCHEDULE FOR #%v]===\n", cpID)

	cpService, err := centralSystem.GetServiceOf(cpID, ChargepointVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.GetCompositeSchedule{
			ChargingRateUnit: rateUnit,
			ConnectorId:      connectorID,
			Duration:         duration,
		})

		if err == nil {
			resp := cpResp.(*csresp.ReserveNow)
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
			fmt.Printf("== RESPUESTA MENSAJE: %v\n", resp)

			if resp.Status == "Accepted" {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			} else {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return response
}

//ChangeAvailability ... [16]
func ChangeAvailability(data ChangeAvailabilityStructure) Response {
	var response Response
	fmt.Printf("\n===[CHANGING AVAILABILITY FOR #%v]===\n", data.MachineID)

	cpService, err := centralSystem.GetServiceOf(data.MachineID, ChargepointVersion, "")

	if err == nil {
		//Inoperative = Charge point is not available for charging. || Operative = Charge point is available for charging
		cpResp, err := cpService.Send(&csreq.ChangeAvailability{
			ConnectorId: data.ConnectorID,
			Type:        data.Type,
		})

		if err == nil {
			resp := cpResp.(*csresp.ReserveNow)
			cs.AddLog(data.MachineID, fmt.Sprintf("%v", resp))
			fmt.Printf("== RESPUESTA MENSAJE: %v\n", resp)

			if resp.Status == "Accepted" {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			} else {
				response = Response{
					Code:         201,
					Message:      resp.Status,
					JSONResponse: resp,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return response
}