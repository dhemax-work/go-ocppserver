package main

import (
	"fmt"
	socketio "github.com/googollee/go-socket.io"
	"github.com/rs/cors"
	"log"
	"net/http"
	"time"
)

func StartSocketServer(){
	fmt.Printf("\n== [Starting Socket Server (SS)] ==\n")
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}

	server.OnConnect("/", func(s socketio.Conn) error {
		s.SetContext("")
		fmt.Println("Connected: ", s.ID())
		return nil
	})

	server.OnDisconnect("/", func(s socketio.Conn, reason string) {
		fmt.Println("closed", reason)
	})

	fmt.Printf("\n == Loaded Events \n")
	time.Sleep(time.Second * 2)

	go server.Serve()
	defer server.Close()


	mux := http.NewServeMux()
	mux.Handle("socket-ocpp", server)
	mux.Handle("/", http.FileServer(http.Dir("./asset")))

	fmt.Printf("\n == Loaded Handlers \n")
	time.Sleep(time.Second * 2)

	fmt.Printf("\n == Initializing Server... \n")

	go func() {
		http.ListenAndServe(":8075", cors.AllowAll().Handler(mux))
	}()

	time.Sleep(time.Second * 2)
	fmt.Print("\n == Server Started (8075)... \n")

	time.Sleep(time.Second * 1)
}