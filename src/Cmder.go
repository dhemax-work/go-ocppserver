package main

import (
	"bufio"
	"fmt"
	"github.com/briandowns/spinner"
	"github.com/eduhenke/go-ocpp/cs"
	"os"
	"strings"
	"time"
)

func StartingCMDer(LoadUI bool) {

	/*
	Load Modules
	*This section is executed in order to start the services using variables of
	the main services started. If the time rules are not followed, it could get corrupted and break the program.
	 */

	s := spinner.New(spinner.CharSets[35], 500*time.Millisecond)
	s.Prefix = "Starting Modules..."
	s.Start()
	time.Sleep(1 * time.Second)
	PreLoad()
	time.Sleep(2 * time.Second)
	s.Stop()

	s.Prefix = "Starting WebService..."
	s.Start()
	OCPPLogs = nil
	StartHTPPServer()
	time.Sleep(3 * time.Second)
	CallClear()
	s.Stop()

	s.Prefix = "Starting WebSocket..."
	s.Start()
	StartSocketServer()
	time.Sleep(1 * time.Second)
	CallClear()
	s.Stop()

	if LoadUI {
		s.Prefix = "Starting CMDer..."
		s.Start()
		time.Sleep(2 * time.Second)
		OCPPLogs = nil
		s.Stop()

		LoadPrettyConsole()
		for {
			time.Sleep(time.Millisecond * 500)
			AddOCPPLog("SYSTEM", "Updating...")
			machineList.RemoveItems()

			for _, m := range cs.ConnectedMachines {
				machineList.AddItems(m)
			}

			for _, m := range cs.ReturnAllLogs() {
				AddOCPPLog("OCPP", m)
			}
			cs.EmptyLogs()
		}
	}

	s.Prefix = "Starting OCPP Server..."
	s.Start()
	time.Sleep(3 * time.Second)
	CallClear()
	StartOCPPServer(ServerOCPPPort)
	s.Stop()
	time.Sleep(2 * time.Second)
	ReadCommand()
}

func PreLoad() {
	AddOCPPLog(" ", " ")
	AddOCPPLog(" ", " ")
}

// ReadCommand, this is for loop listen the input in console
func ReadCommand() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter command: ")
	text, _ := reader.ReadString('\n')
	if isWindows {
		text = strings.Replace(text, "\r\n", "", -1)
	} else {
		text = strings.Replace(text, "\n", "", -1)
	}
	ExecuteNoUICommand(text)
}


/*
ExecuteNoUICommand
Custom commands for the server
 */
func ExecuteNoUICommand(command string) {
	if strings.Compare("-help", command) == 0 {
		fmt.Printf("Command List:\n" +
			"- machine_list\n")
	} else if strings.Compare("machine_list", command) == 0 {
		GetConnectedMachines()
	} else if strings.Compare("quit", command) == 0 {
		fmt.Println("Shutting down")
		time.Sleep(time.Second * 2)
		os.Exit(200)
	} else {
		fmt.Printf("Try '-help' command\n")
	}
	ReadCommand()
}

func ExecuteUICommand(command string) {
	switch command {
	case "machine:list":

	default:

	}
}
