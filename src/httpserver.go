package main

import (
	"encoding/json"
	"fmt"
	"github.com/eduhenke/go-ocpp/cs"
	"github.com/rs/cors"
	"log"
	"net/http"
	"strconv"
	"time"
)

/* VARIABLES */
//Response
var response Response

// isWebServerOnline ...
var isWebServerOnline = false

var InvalidMethod = "Invalid Method"
var OfflineMachine = "Charge Point not connected"

//StartHTTPServer ... start API
func StartHTPPServer() {
	if !isWebServerOnline {
		fmt.Printf("\n== [Starting Web Server (WS)] ==\n")
		time.Sleep(time.Second * 1)
		mux := http.NewServeMux()
		fmt.Printf("\n== [WS: [38%%] Load Mux...] ==\n")

		mux.HandleFunc("/api/v1/machines", GetMachinesHTPP)             //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine", GetStatusHTTP)                //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine/logger", GetMachineLogHTTP)     //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine/list", GetLocalListHTTP)        //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine/message", TriggerMessageHTTP)   //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine/diagnostic", GetDiagnosticHTTP) //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine/config", GetConfigurationHTTP)  //GET [CHECKED]
		mux.HandleFunc("/api/v1/machine/schedule", GetScheduleHTTP)

		mux.HandleFunc("/api/v1/machine/cache", ClearCacheHTTP) //DELETE [CHECKED]

		mux.HandleFunc("/api/v1/machine/unlock", UnlockConnectorHTTP)               //POST [CHECKED]
		mux.HandleFunc("/api/v1/machine/reset", ResetMachineHTTP)                   //POST [CHECKED]
		mux.HandleFunc("/api/v1/machine/config/set", ChangeConfigurationHTTP)        //POST [CHECKED]
		mux.HandleFunc("/api/v1/machine/stop", RemoteStopHTTP)                      //POST [CHECKED]
		mux.HandleFunc("/api/v1/machine/start", RemoteStartHTTP)                    //POST [CHECKED]
		mux.HandleFunc("/api/v1/machine/update", UpdateFirmwareHTTP)                //POST [CHECKED]
		mux.HandleFunc("/api/v1/machine/profile", UpdateProfileHTTP)                //POST
		mux.HandleFunc("/api/v1/machine/reservation/create", ReserveNowHTTP)        //POST
		mux.HandleFunc("/api/v1/machine/reservation/cancel", CancelReservationHTTP) //POST
		mux.HandleFunc("/api/v1/machine/availability/change", ChangeAvailabilityHTTP) //POST

		fmt.Printf("\n== [WS: [64%%] Loaded Methods ...] ==\n")
		time.Sleep(time.Second * 2)
		handler := cors.AllowAll().Handler(mux)
		fmt.Printf("\n== [WS: [73%%] CORS Loaded...] ==\n")

		time.Sleep(time.Second * 1)

		go func() {
			log.Fatal(http.ListenAndServe(":8081", handler))
		}()

		fmt.Printf("\n== [WS: [100%%] Server Started (port 8081)...] ==\n")
		isWebServerOnline = true
	}
}

//Response ...
type Response struct {
	Code         int         `json:"code"`
	Message      string      `json:"message"`
	JSONResponse interface{} `json:"response"`
}

func setupResponse(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	(*w).Header().Set("Content-Type", "application/json")
}

/* STRUCTURES */

type UnlockConnectorStructure struct {
	MachineID   string `json:"machineID"`
	ConnectorID int    `json:"connectorID"`
}
type GetStatusStructure struct {
	MachineID string `json:"machineID"`
}
type ResetMachineStructure struct {
	ResetType string `json:"reset_type"`
	MachineID string `json:"machineID"`
}
type ChangeConfigurationStructure struct {
	MachineID string `json:"machineID"`
	Key       string `json:"key"`
	Value     string `json:"value"`
}
type RemoteStopStructure struct {
	MachineID     string `json:"machineID"`
	TransactionID int32  `json:"transactionID"`
}
type RemoteStartStructure struct {
	MachineId              string `json:"machineID"`
	TagID                  string `json:"tagID"`
	ConnectorID            int    `json:"connectorID"`
	TransactionId          int    `json:"transactionID"`
	ChargingProfileId      int    `json:"profileID"`
	ChargingProfileKind    string `json:"profileKind"`
	ChargingProfilePurpose string `json:"profilePurpose"`

	ChargingscheduleDuration           int     `json:"scheduleDuration"`
	ChargingscheduleChargingrateunit   string  `json:"scheduleRateUnit"`
	ChargingscheduleperiodStartperiod  int     `json:"scheduleStartPeriod"`
	ChargingscheduleperiodLimit        float64 `json:"scheduleLimit"`
	ChargingscheduleperiodNumberphases int     `json:"scheduleNumberPhases"`

	ChargingMinChargingRate int    `json:"scheduleMinChargingRate"`
	ChargingStartSchedule   string `json:"scheduleStartSchedule"`
	ChargingRecurrencyKind  string `json:"scheduleRecurrencyKind"`
	ChargingStackLevel      int    `json:"scheduleStackLevel"`
	ChargingValidForm       string `json:"scheduleValidForm"`
	ChargingValidTo         string `json:"scheduleValidTo"`
}
type UpdateFirmwareStructure struct {
	MachineId     string    `json:"machineID"`
	Location      string    `json:"tagID"`
	Retries       float64   `json:"retries"`
	RetrieveDate  time.Time `json:"retrieveDate"`
	RetryInterval float64   `json:"retryInterval"`
}

type ReserveNowStructure struct {
	MachineID     string    `json:"machineID"`
	ConnectorID   int       `json:"connectorID"`
	ExpiryDate    time.Time `json:"expiryDate"`
	IdTag         string    `json:"idTAG"`
	ParentIdTag   string    `json:"parentIdTag"`
	ReservationID int       `json:"reservationID"`
}

type ChangeAvailabilityStructure struct {
	MachineID string `json:"machineID"`
	ConnectorID int `json:"connectorID"`
	Type string `json:"type"`
}

//GetMachinesHTTP [1]
func GetMachinesHTPP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if len(cs.ConnectedMachines) > 0 {
			response = Response{
				Code:         200,
				Message:      fmt.Sprintf("Machines connected: %v", len(cs.ConnectedMachines)),
				JSONResponse: cs.ConnectedMachines,
			}
		} else {
			response = Response{
				Code:    200,
				Message: fmt.Sprintf(OfflineMachine),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetStatusHTTP [2]
func GetStatusHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {
			MachineID := r.FormValue("machineID")
			if cs.IsMachineConnect(MachineID) {
				response = GetMachineStatus(MachineID)
			} else {
				response = Response{
					Code:    0,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetMachineLogHTTP [3]
func GetMachineLogHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {

			machineID := r.FormValue("machineID")
			if cs.IsMachineConnect(machineID) {
				if len(machineID) < 1 {
					response = Response{
						Code:    1,
						Message: OfflineMachine,
					}
				} else {
					response = Response{
						Code:         200,
						Message:      "Machine Logs",
						JSONResponse: cs.ReturnLogs()[machineID],
					}
				}
			} else {
				response = Response{
					Code:         200,
					Message:      "List of all logs",
					JSONResponse: cs.ReturnAllLogs(),
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetLocalListHTTP [4]
func GetLocalListHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {
			cpIDVar := r.FormValue("machineID")
			response = GetLocalList(cpIDVar)
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//TriggerMessageHTTP [5]
func TriggerMessageHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {
			cpIDVar := r.FormValue("machineID")
			connector := r.FormValue("connectorID")
			msg := r.FormValue("requestMessage")
			s := connector
			if number, err := strconv.Atoi(s); err == nil {
				response = TriggerMessage(cpIDVar, number, msg)
			} else {
				response = Response{
					Code:    301,
					Message: "ConnectorID must be integer",
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetDiagnosticHTTP [6]
func GetDiagnosticHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {
			cpIDVar := r.FormValue("machineID")

			location := r.FormValue("location")
			retries := r.FormValue("retries")
			interval := r.FormValue("interval")

			startTime := r.FormValue("startTime")
			endTime := r.FormValue("endTime")

			if ret, err := strconv.Atoi(retries); err == nil {
				if inter, err := strconv.Atoi(interval); err == nil {
					if cs.IsMachineConnect(cpIDVar) {
						response = GetDiagnostic(cpIDVar, location, ret, inter, ToTimestamp(startTime), ToTimestamp(endTime))
					} else {
						response = Response{
							Code:    1,
							Message: OfflineMachine,
						}
					}
				} else {
					response = Response{
						Code:         301,
						Message:      "Interval must be Integer",
						JSONResponse: nil,
					}
				}
			} else {
				response = Response{
					Code:         301,
					Message:      "Retries must be Integer",
					JSONResponse: nil,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetConfigurationHTTP [5]
func GetConfigurationHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {
			key := r.FormValue("key")
			machineID := r.FormValue("machineID")
			if cs.IsMachineConnect(machineID) {
				if len(key) < 1 {
					response = Response{
						Code:    201,
						Message: "Key must be string non empty",
					}
				} else {
					SendKey := []string{key}
					response = GetConfiguration(machineID, SendKey)
				}
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetScheduleHTTP [16]
func GetScheduleHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {

			rate := r.FormValue("chargingRate")
			connector := r.FormValue("connectorID")
			duration := r.FormValue("duration")
			machineID := r.FormValue("machineID")

			if durat, err := strconv.Atoi(duration); err == nil {
				if connect, err := strconv.Atoi(connector); err == nil {
					if cs.IsMachineConnect(machineID) {
						response = GetCompositeSchedule(machineID, rate, connect, durat)
					} else {
						response = Response{
							Code:    1,
							Message: OfflineMachine,
						}
					}
				} else {
					response = Response{
						Code:         301,
						Message:      "Interval must be Integer",
						JSONResponse: nil,
					}
				}
			} else {
				response = Response{
					Code:         301,
					Message:      "Retries must be Integer",
					JSONResponse: nil,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//ClearCacheHTTP [6]
func ClearCacheHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	var response Response

	if r.Method == "DELETE" {

		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {

			var result GetStatusStructure
			json.Unmarshal(bytes, &result)

			cpIDVar := result.MachineID
			if cs.IsMachineConnect(cpIDVar) {
				response = ClearCache(cpIDVar)
			} else {
				response = Response{
					Code:    0,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//UnlockConnectorHTTP [7]
func UnlockConnectorHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result UnlockConnectorStructure
			json.Unmarshal(bytes, &result)

			connectorID := result.ConnectorID
			machineID := result.MachineID

			if cs.IsMachineConnect(machineID) {
				if connectorID < 1 {
					response = Response{
						Code:    201,
						Message: "Connector ID must be > 1",
					}
				} else {
					response = UnlockConnector(machineID, connectorID)
				}
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//ResetMachineHTTP [8]
func ResetMachineHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {

		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result ResetMachineStructure
			json.Unmarshal(bytes, &result)

			resetType := result.ResetType
			machineID := result.MachineID
			if len(resetType) < 1 {
				response = Response{
					Code:    301,
					Message: "Argument length short",
				}
			} else {
				if resetType != "" {
					if cs.IsMachineConnect(machineID) {
						response = ResetMachine(machineID, resetType)
					} else {
						response = Response{
							Code:    1,
							Message: OfflineMachine,
						}
					}
				} else {
					response = Response{
						Code:    301,
						Message: "Invalid Argument",
					}
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//ChangeConfigurationHTTP [9]
func ChangeConfigurationHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result ChangeConfigurationStructure
			json.Unmarshal(bytes, &result)

			key := result.Key
			value := result.Value
			machineID := result.MachineID

			if cs.IsMachineConnect(machineID) {
				if len(key) < 1 || len(value) < 1 {
					response = Response{
						Code:    201,
						Message: "Values are empty",
					}
				} else {
					response = ChangeConfiguration(machineID, key, value)
				}
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//RemoteStopHTTP [10]
func RemoteStopHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result RemoteStopStructure
			json.Unmarshal(bytes, &result)
			machineID := result.MachineID

			transaction := result.TransactionID

			if cs.IsMachineConnect(machineID) {
				response = RemoteStop(machineID, transaction)
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//RestartHTTP [11]
func RemoteStartHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result RemoteStartStructure
			json.Unmarshal(bytes, &result)

			if result.ConnectorID > 1 || result.TransactionId > 1 || result.ChargingProfileId > 1 {
				if cs.IsMachineConnect(result.MachineId) {
					response = RemoteStart(result)
				} else {
					response = Response{
						Code:    1,
						Message: OfflineMachine,
					}
				}
			} else {
				response = Response{
					Code:         201,
					Message:      "Some values must be major that zero.",
					JSONResponse: nil,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//UpdateFirmwareHTTP [12]
func UpdateFirmwareHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result UpdateFirmwareStructure
			json.Unmarshal(bytes, &result)
			//TODO Finish the validation params

			if cs.IsMachineConnect(result.MachineId) {
				response = UpdateFirmware(result)
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

//UpdateProfileHTTP [13]
func UpdateProfileHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result RemoteStartStructure
			json.Unmarshal(bytes, &result)
			//TODO Finish the validation

			if cs.IsMachineConnect(result.MachineId) {
				response = SetChargingProfile(result)
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

func ReserveNowHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result ReserveNowStructure
			json.Unmarshal(bytes, &result)
			//TODO Finish the validation

			if cs.IsMachineConnect(result.MachineID) {
				response = ReserveNow(result)
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}

func CancelReservationHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result ReserveNowStructure
			json.Unmarshal(bytes, &result)
			//TODO Finish the validation

			if cs.IsMachineConnect(result.MachineID) {
				response = CancelReservation(result)
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}


func ChangeAvailabilityHTTP(w http.ResponseWriter, r *http.Request){
	setupResponse(&w)
	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result ChangeAvailabilityStructure
			json.Unmarshal(bytes, &result)
			//TODO Finish the validation

			if cs.IsMachineConnect(result.MachineID) {
				response = ChangeAvailability(result)
			} else {
				response = Response{
					Code:    1,
					Message: OfflineMachine,
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: InvalidMethod,
		}
	}
	json.NewEncoder(w).Encode(response)
}