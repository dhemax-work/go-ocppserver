package main

import (
	"bytes"
	"fmt"
	"github.com/eduhenke/go-ocpp/cs"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"time"
)

//ToTimeStamp ...
func ToTimestamp(timeToConvert string) time.Time {
	i, err := strconv.ParseInt(timeToConvert, 10, 64)
	var tm time.Time
	if err == nil {
		tm = time.Unix(i, 0)
	}
	return tm
}

//BodyToJson ...
func BodyToJson(r *http.Request) []byte {
	bodyB, _ := ioutil.ReadAll(r.Body)
	bodyStr := string(bytes.Replace(bodyB, []byte("\r"), []byte("\r\n"), -1))
	bytesreturned := []byte(bodyStr)

	return bytesreturned
}

func GetConnectedMachines() {
	fmt.Printf("\n ===== [STATUS DE MAQUINAS] =====\n\n")
	if len(cs.ConnectedMachines) > 0 {
		if !cs.HaveEmpty() {
			fmt.Printf("\n== [MAQUINAS CONECTADAS]==\n\t%v\n\n", cs.ListMachines())
		}
	} else {
		fmt.Printf("\nLOG: No hay maquinas conectadas\n")
	}
	fmt.Printf("====================================\n\n")
}

var clear map[string]func() //create a map for storing clear funcs

func init() {
	clear = make(map[string]func()) //Initialize it
	clear["linux"] = func() {
		cmd := exec.Command("clear") //Linux example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

// Clear Console Logs (Only screen)
func CallClear() {
	value, ok := clear[runtime.GOOS] //runtime.GOOS -> linux, windows, darwin etc.
	if ok {                          //if we defined a clear func for that platform:
		value() //we execute it
	} else { //unsupported platform
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}
