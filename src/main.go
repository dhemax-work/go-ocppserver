package main

import (
	"runtime"
)

/**
	CODES:
	0: Offline server
	1: Machine not available
	200: Success response
	201: Rejected response
	300: Invalid parameters
	301: Internal Error
**/

/* VARIABLES */
//machineStatus ... get the status of charge point
var machineStatus []string

//TODO Implement UI interface 'not necessary'
var WithUI = false

/*
OCPP Server Config
- Main server OCCP Port
- Check if OS is Linux/Windows
 */
var ServerOCPPPort = "8090"
var isWindows = false

func DetectOS() {
	if runtime.GOOS == "windows" {
		isWindows = true
	}
}

// Running main services for the OCPP server
func main() {
	DetectOS()
	StartingCMDer(false)
	//StartOCPPServer(ServerOCPPPort)
	select {}
}
