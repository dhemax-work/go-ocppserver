package main

import (
	"fmt"
	"github.com/marcusolsson/tui-go"
	_ "io/ioutil"
	"log"
	_ "net/http"
	_ "sort"
	_ "strings"
	"time"
)

var socketList tui.List
var apiList tui.List
var machineList tui.List

var historySocket = tui.NewVBox()
var history = tui.NewVBox()

type post struct {
	username string
	message  string
	time     string
}

var OCPPLogs []post

var SocketLogs []post

var SocketList []string

func AddMachine(cpIDs []string) {
	for _, m := range cpIDs {
		machineList.AddItems(m)
	}
}

func RemoveMachine(cpID string, cpIDs []string) {
	machineList.RemoveItems()
	for _, m := range cpIDs {
		machineList.AddItems(m)
	}
}

func LoadOCPPLogs() {
	for _, m := range OCPPLogs {
		history.Append(tui.NewHBox(
			tui.NewLabel(m.time),
			tui.NewPadder(1, 0, tui.NewLabel(fmt.Sprintf("<%s>", m.username))),
			tui.NewLabel(m.message),
			tui.NewSpacer(),
		))
	}
}

func AddOCPPLog(typeKey string, message string) {
	var newPost = post{
		username: typeKey,
		message:  message,
		time:     time.Now().Format("15:04"),
	}
	OCPPLogs = append(OCPPLogs, newPost)
	LoadOCPPLogs()
}

func LoadSocketLogs() {
	for _, m := range SocketLogs {
		time.Sleep(time.Millisecond * 300)
		historySocket.Append(tui.NewHBox(
			tui.NewLabel(m.time),
			tui.NewPadder(1, 0, tui.NewLabel(fmt.Sprintf("<%s>", m.username))),
			tui.NewLabel(m.message),
			tui.NewSpacer(),
		))
	}
}

func AddSocketLog(typeKey string, message string) {
	var newPost = post{
		username: typeKey,
		message:  message,
		time:     time.Now().Format("15:04"),
	}
	SocketLogs = append(SocketLogs, newPost)
	LoadSocketLogs()
}

func AddSocketList(command string) {
	socketList.AddItems(command)
}

func LoadPrettyConsole() {
	historyScroll := tui.NewScrollArea(history)
	historyScroll.SetAutoscrollToBottom(true)

	historyBox := tui.NewVBox(historyScroll)
	historyBox.SetTitle("OCPP Logs")
	historyBox.SetBorder(true)

	historySocketScroll := tui.NewScrollArea(historySocket)
	historySocketScroll.SetAutoscrollToBottom(true)

	historySocketBox := tui.NewVBox(historySocketScroll)

	socketConnections := tui.NewVBox(&socketList)
	socketConnections.SetTitle("Socket Connections")
	socketConnections.SetBorder(true)
	socketConnections.SetSizePolicy(tui.Expanding, tui.Preferred)

	machineConnections := tui.NewVBox(&machineList)
	machineConnections.SetTitle("Machines")
	machineConnections.SetBorder(true)

	apiConnections := tui.NewVBox(&apiList)
	apiConnections.SetTitle("API")
	apiConnections.SetBorder(true)

	respHeadLbl := tui.NewLabel("")
	respHeadLbl.SetSizePolicy(tui.Expanding, tui.Expanding)

	SocketLogs := tui.NewVBox(historySocketBox)
	SocketLogs.SetTitle("Socket Logs")
	SocketLogs.SetBorder(true)
	socketList.AddItems("                               ")

	input := tui.NewEntry()
	input.SetFocused(true)
	input.SetSizePolicy(tui.Expanding, tui.Maximum)

	inputBox := tui.NewHBox(input)
	inputBox.SetBorder(true)
	inputBox.SetTitle("Command")
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	req := tui.NewVBox(socketConnections, machineConnections, apiConnections)
	req.SetSizePolicy(tui.Maximum, tui.Preferred)

	resp := tui.NewVBox(SocketLogs, historyBox, inputBox)
	resp.SetSizePolicy(tui.Expanding, tui.Preferred)

	panel := tui.NewHBox(req, resp)
	panel.SetSizePolicy(tui.Preferred, tui.Expanding)

	input.OnSubmit(func(e *tui.Entry) {
		AddOCPPLog("COMMAND", e.Text())
		ExecuteUICommand(e.Text())
		input.SetText("")
	})

	root := tui.NewVBox(panel)

	tui.DefaultFocusChain.Set(&socketList, machineConnections, apiConnections)

	theme := tui.NewTheme()
	theme.SetStyle("box.focused.border", tui.Style{Fg: tui.ColorYellow, Bg: tui.ColorDefault})

	ui, err := tui.New(root)
	if err != nil {
		log.Fatal(err)
	}

	ui.SetTheme(theme)
	ui.SetKeybinding("Esc", func() { ui.Quit() })

	if err := ui.Run(); err != nil {
		log.Fatal(err)
	}
}
